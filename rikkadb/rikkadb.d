module rikkadb.rikkadb;

import rikkadb.col;

import std.conv;
import std.file;
import std.stdio;
import std.string;


class RikkaDB {

  string dir;
  Col[string] strCol;

  this(string _dir) {
    mkdirRecurse(_dir);
    dir = _dir;

    foreach (string f; dirEntries(dir, SpanMode.breadth)) {
      if (isDir(f)) {
	strCol[f] = new Col([dir, f].join("/"));
      }
      // Successfully opened collection
    }
  }

  // Create a new collection
  void create(string name) {
    if (name in strCol) {
      writeln("already exists");
      return;
    }
    strCol[name] = new Col([dir, name].join("/"));
  }

  // return collection
  Col use(string name) {
    if (name in strCol) {
      return strCol[name];
    }
    return null;
  }

  // rename
  // void rename(string oldName, string newName) {
  //   if (oldName !in strCol || newName in strCol) {
  //     assert(0);
  //   }
  //   auto col = strCol[oldName];
  //   col.close();
  //   strCol.remove(oldName);
  //   std.file.rename([dir, oldName].join("/"), [dir, newName].join("/"));
  //   strCol[newName] = new Col([dir, newName].join("/"));
  // }

  // drop a collection
  void drop(string name) {
    if (name in strCol) {
      strCol[name].close();
      strCol.remove(name);
      rmdirRecurse([dir, name].join("/"));
    } else {
      writeln("There's no collection such name.");
    }
  }

  // Flush all collection data files
  void flush() {
    foreach(col; strCol) {
      col.flush();
    }
  }

  // close all collections
  void close() {
    foreach (col; strCol) {
      col.close();
    }
  }
  
}


unittest {
  string tmp = "/tmp/rikka_db_test";

  void testCRUD() {
    if (exists(tmp) && isDir(tmp)) {
      rmdirRecurse(tmp);
    }
    scope(exit) {
      if (exists(tmp) && isDir(tmp)) {
	rmdirRecurse(tmp);
      }
    }

    auto db = new RikkaDB(tmp);
    scope(exit) db.close();

    // create
    db.create("a");
    db.create("b");

    // use
    db.use("a");
    db.use("b");

    // rename
    // db.rename("a", "c");

    // drop
    db.drop("a");

    assert(db.strCol.keys == ["b"]);

    db.flush();
  }

  testCRUD();
}


//version(unittest) void main() {}