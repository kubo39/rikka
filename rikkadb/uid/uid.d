import std.stdio, std.random, std.format, std.array;


// generate new unique identifier
string nextUID() {
  int[] arr = [uniform(0, 2<<7), uniform(0, 2<<7),
	       uniform(0, 2<<7), uniform(0, 2<<7),
	       uniform(0, 2<<7), uniform(0, 2<<7),
	       uniform(0, 2<<7), uniform(0, 2<<7),
	       uniform(0, 2<<7), uniform(0, 2<<7),
	       uniform(0, 2<<7), uniform(0, 2<<7),
	       uniform(0, 2<<7), uniform(0, 2<<7),
	       uniform(0, 2<<7), uniform(0, 2<<7)];

  auto writer = Appender!string([]);
  formattedWrite(writer, "%(%X%)", arr);
  return writer.data;
}


unittest {
  auto str = nextUID();
  assert(32 == str.length);
}


version(unittest) void main() {}